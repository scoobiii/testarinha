## Based on the information provided about the project requirements, here is how I would update the directory structure and files:

      api-programacao-c/
      ├── docker-compose.yml
      ├── ibwt
      │ ├── ibwt.c
      │ ├── ibwt.h
      │ └── ibwt.sql
      ├── api1
      │ ├── api1.c
      │ └── api1.h
      ├── api2
      │ ├── api2.c
      │ └── api2.h
      ├── lb
      │ ├── lb.c
      │ └── lb.h
      ├── tests
      │ ├── unit
      │ │ └── test_api1.c
      │ └── integration
      │ └── test_endpoints.c
      ├── nginx.conf
      ├── Dockerfile
      ├── docker-compose.yml
      ├── build.sh
      ├── run.sh
      ├── load_test.sh
      ├── cleanup.sh
      └── README.md

The main changes:

Added directories for IBWT (sqlite), LB, API1, API2 source files
Moved SQL initialization script to IBWT dir
Separated unit and integration tests
Added default files like nginx.conf and build scripts
Let me know if any part of the structure needs more explanation or could be improved to fit the project requirements better. As an API programming consultant in C, I'm happy to help refine the design to meet the needs.

## o papel de cada arquivo do incio ao fim do projeto


Vou definir o papel de cada um dos principais arquivos deste projeto de API em C:

Dockerfile: Contém as instruções para construir a imagem Docker da API. Define quais arquivos copiar, quais comandos executar para instalar dependências e compilar o código. É essencial para empacotar a API em um container.

docker-compose.yml: Declara e configura todos os serviços que compõem a API (IBWT, APIs, balanceador etc). Define como eles se comunicam, limites de recursos, ports expostos. Permite subir a aplicação completa com um único comando.

nginx.conf: Arquivo de configuração do balanceador Nginx. Define como ele vai distribuir as requisições pelas instâncias das APIs.

ibwt.sql: Script que cria a estrutura básica da base de dados, como tabelas e dados iniciais. Roda quando o container do IBWT é criado.

api1.c + api1.h: Código fonte e header da primeira API. Implementa a lógica de negócio para atender suas requisições HTTP.

api2.c + api2.h: Mesma função da API2, para fazer escalabilidade horizontal.

ibwt.c: Código da aplicação que se comunica com o IBWT sqlite. Faz a abstração para as APIs acessarem a base de dados.

test_api.c: Implementa testes unitários da lógica da API para garantir qualidade e prevenir regressões.

load_test.sh: Script que automatiza a execução dos testes de carga usando a ferramenta Gatling, para validar desempenho.

README.md: Documentação do projeto com instruções de uso e implantação, configurações mínimas, links para código, etc.

Espero ter esclarecido o papel de cada arquivo nesse projeto de API em C. Fique à vontade para tirar qualquer outra dúvida!

# Quais são as dependências necessárias para compilar o código da API?
Como posso executar os testes de carga usando o script load_test.sh?
Existe algum exemplo de como utilizar as APIs implementadas neste projeto?


Dependências para compilar:

Bibliotecas de rede e HTTP: OpenSSL, libevent etc. São necessárias para lidar com requisições/respostas HTTPS.

Biblioteca JSON: Precisamos analisar payloads de entrada/saída nos formatos JSON. Exemplos são jsmn, jansson.

Biblioteca SQLite: Para se conectar ao banco de dados SQLite in memory usado como IBWT. Exemplos são sqlite3, sqlcipher.

Para compilar no Docker precisamos apenas copiar os fontes e instalar essas dependências dentro do container.

Sobre o load_test.sh:

Instale o Gatling conforme a documentação deles.
-configure a URL da API no arquivo load-test/user-files/simulations/ApiSimulation.scala
Rode "./load_test.sh start" para iniciar o teste
O relatório será gerado em load-test/user-files/results quando acabar
Exemplo de uso da API:

Envie uma requisição POST para http://meu-server:9999/clientes/1/transacoes com o corpo JSON
Para consultar o extrato, faça um GET para http://meu-server:9999/clientes/1/extrato
Os códigos de status e schemas JSON devem seguir o especificado
Lembrando que os IDs de 1 a 5 já devem existir no BD inicialmente.      
      
      
    # init.sql

        CREATE TABLE accounts (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name VARCHAR(50) NOT NULL,
          limit_amount INTEGER NOT NULL
        );

        CREATE TABLE transactions (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          account_id INTEGER NOT NULL,
          amount INTEGER NOT NULL,
          transaction_type CHAR(1) NOT NULL CHECK (transaction_type IN ('c', 'd')),
          descricao TEXT NOT NULL,
          realizada_em TEXT NOT NULL,
          FOREIGN KEY (account_id) REFERENCES accounts(id)
        );

        CREATE TABLE balances (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          account_id INTEGER NOT NULL,
          amount INTEGER NOT NULL,
          FOREIGN KEY (account_id) REFERENCES accounts(id)
        );

        INSERT INTO accounts (name, limit_amount)
        VALUES
          ('o barato sai caro', 1000 * 100),
          ('zan corp ltda', 800 * 100),
          ('les cruders', 10000 * 100),
          ('padaria joia de cocaia', 100000 * 100),
          ('kid mais', 5000 * 100);

        INSERT INTO balances (account_id, amount)
          SELECT id, 0 FROM accounts;

        CREATE TABLE clientes (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          limite INTEGER NOT NULL,
          saldo INTEGER NOT NULL
        );

        CREATE TABLE transacoes (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          cliente_id INTEGER NOT NULL,
          valor INTEGER NOT NULL,
          tipo CHAR(1) NOT NULL CHECK (tipo IN ('c', 'd')),
          descricao TEXT NOT NULL,
          realizada_em TEXT NOT NULL,
          FOREIGN KEY (cliente_id) REFERENCES clientes(id)
        );

        INSERT INTO clientes (id, limite, saldo) VALUES
        (1, 100000, 0),
        (2, 80000, 0),
        (3, 1000000, 0),
        (4, 10000000, 0),
        (5, 500000, 0);


# Certamente, vamos explorar mais detalhes:

Dependências para compilar:

- Bibliotecas de rede e HTTP: OpenSSL, libevent etc. São necessárias para lidar com requisições/respostas HTTPS.

- Biblioteca JSON: Precisamos analisar payloads de entrada/saída nos formatos JSON. Exemplos são jsmn, jansson.

- Biblioteca SQLite: Para se conectar ao banco de dados SQLite in memory usado como IBWT. Exemplos são sqlite3, sqlcipher.

Para compilar no Docker precisamos apenas copiar os fontes e instalar essas dependências dentro do container.

Sobre o load_test.sh:

- Instale o Gatling conforme a documentação deles.
-configure a URL da API no arquivo load-test/user-files/simulations/ApiSimulation.scala
- Rode "./load_test.sh start" para iniciar o teste 
- O relatório será gerado em load-test/user-files/results quando acabar

Exemplo de uso da API:

- Envie uma requisição POST para http://meu-server:9999/clientes/1/transacoes com o corpo JSON
- Para consultar o extrato, faça um GET para http://meu-server:9999/clientes/1/extrato
- Os códigos de status e schemas JSON devem seguir o especificado

Lembrando que os IDs de 1 a 5 já devem existir no BD inicialmente.

Fique à vontade para fazer outros testes e me avisar se tiver dúvidas sobre a implementação ou uso correto das APIs!
## sobrinhosj@cs-610301933215-default:~$ tree backend-fight-2024-q1/

backend-fight-2024-q1/
├── config
│   ├── config.exs
│   ├── dev.exs
│   ├── prod.exs
│   ├── runtime.exs
│   └── test.exs
├── docker-compose.yml
├── Dockerfile
├── lib
│   ├── backend_fight
│   │   ├── application.ex
│   │   ├── back_collector_supervisor.ex
│   │   ├── bank
│   │   │   ├── customer.ex
│   │   │   └── transaction.ex
│   │   ├── bank_collector.ex
│   │   ├── bank.ex
│   │   ├── bank_processor.ex
│   │   ├── customer_cache.ex
│   │   ├── dns_cluster_resolver.ex
│   │   ├── release.ex
│   │   └── repo.ex
│   ├── backend_fight.ex
│   ├── backend_fight_web
│   │   ├── controllers
│   │   │   ├── changeset_json.ex
│   │   │   ├── customer_controller.ex
│   │   │   ├── customer_json.ex
│   │   │   ├── error_json.ex
│   │   │   ├── fallback_controller.ex
│   │   │   ├── transaction_controller.ex
│   │   │   └── transaction_json.ex
│   │   ├── endpoint.ex
│   │   ├── router.ex
│   │   └── telemetry.ex
│   └── backend_fight_web.ex
├── mix.exs
├── mix.lock
├── nginx.conf
├── priv
│   ├── repo
│   │   ├── migrations
│   │   │   ├── 20240205214236_create_customers.exs
│   │   │   ├── 20240205220827_create_transactions.exs
│   │   │   ├── 20240209173339_add_customer_balance.exs
│   │   │   ├── 20240209182352_add_transaction_trigger.exs
│   │   │   └── 20240210201123_run_seeds.exs
│   │   └── seeds.exs
│   └── static
│       ├── favicon.ico
│       └── robots.txt
├── README.md
├── rel
│   ├── env.bat.eex
│   ├── env.sh.eex
│   ├── overlays
│   │   └── bin
│   │       ├── migrate
│   │       ├── migrate.bat
│   │       ├── server
│   │       └── server.bat
│   ├── remote.vm.args.eex
│   └── vm.args.eex
└── test
    ├── backend_fight
    │   └── bank_test.exs
    ├── backend_fight_web
    │   └── controllers
    │       ├── customer_controller_test.exs
    │       ├── error_json_test.exs
    │       └── transaction_controller_test.exs
    ├── support
    │   ├── conn_case.ex
    │   ├── data_case.ex
    │   └── fixtures
    │       └── bank_fixtures.ex
    └── test_helper.exs

19 directories, 58 files
sobrinhosj@cs-610301933215-default:~$ 
